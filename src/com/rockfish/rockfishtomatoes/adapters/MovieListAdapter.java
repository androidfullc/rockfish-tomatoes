package com.rockfish.rockfishtomatoes.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.rockfish.rockfishtomatoes.ImageCache;
import com.rockfish.rockfishtomatoes.R;
import com.rockfish.rockfishtomatoes.model.Movie;

public class MovieListAdapter extends BaseAdapter {

    List<Movie> _movies;

    public MovieListAdapter(Context context, List<Movie> movies) {
        super();
        this._movies = movies;
    }

    @Override
    public int getCount() {
        return (_movies != null) ? _movies.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return (_movies != null) ? _movies.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return (_movies != null) ? _movies.get(position).hashCode() : 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater li = LayoutInflater.from(parent.getContext());
            convertView = li.inflate(R.layout.row_movie_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Movie movie = _movies.get(position);

        holder.movieThumb.setImageDrawable(ImageCache.getInstance().getImageFromCache(movie.getPosters().thumbnail));
        holder.movieTitle.setText(movie.getMovieTitle());
        holder.movieRating.setRating((float) movie.getRatings().critics_score / 20);
        holder.mpaaRating.setText(movie.getMpaaRating());
        if (movie.getMpaaRating().equalsIgnoreCase("G")) {
            holder.mpaaRating.setTextColor(parent.getContext().getResources().getColor(R.color.green));
        } else if (movie.getMpaaRating().equalsIgnoreCase("PG")) {
            holder.mpaaRating.setTextColor(parent.getContext().getResources().getColor(R.color.yellow));
        } else if (movie.getMpaaRating().equalsIgnoreCase("PG-13")) {
            holder.mpaaRating.setTextColor(parent.getContext().getResources().getColor(R.color.orange));
        } else if (movie.getMpaaRating().equalsIgnoreCase("R")) {
            holder.mpaaRating.setTextColor(parent.getContext().getResources().getColor(R.color.red));
        } else {
            holder.mpaaRating.setTextColor(parent.getContext().getResources().getColor(android.R.color.black));
        }

        return convertView;
    }

    public class ViewHolder {
        ImageView movieThumb;
        TextView movieTitle;
        RatingBar movieRating;
        TextView mpaaRating;

        /**
         * A constructor that takes a View will encourage the next person to do the right thing with the costly call to
         * findViewById()
         * 
         * @param v
         */
        ViewHolder(View v) {
            this.movieThumb = (ImageView) v.findViewById(R.id.movieThumb);
            this.movieTitle = (TextView) v.findViewById(R.id.movieTitle);
            this.movieRating = (RatingBar) v.findViewById(R.id.movieRating);
            this.mpaaRating = (TextView) v.findViewById(R.id.mpaaRating);
        }
    }

}
