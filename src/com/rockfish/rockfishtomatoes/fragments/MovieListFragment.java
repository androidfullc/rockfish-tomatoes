package com.rockfish.rockfishtomatoes.fragments;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.Gson;
import com.rockfish.rockfishtomatoes.ImageCache;
import com.rockfish.rockfishtomatoes.R;
import com.rockfish.rockfishtomatoes.adapters.MovieListAdapter;
import com.rockfish.rockfishtomatoes.model.Movie;
import com.rockfish.rockfishtomatoes.model.MovieResponse;
import com.rockfish.rockfishtomatoes.util.Base64;

public class MovieListFragment extends ListFragment {

    public static final String TAG = MovieListFragment.class.getSimpleName();
    private OnItemSelectedListener _listener;
    private GetMovieListAsyncTask _getMovieListAsync;
    private MovieListAdapter _mla;
    ArrayList<Movie> _movies;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey("movies")) {
            _movies = savedInstanceState.getParcelableArrayList("movies");
            if (_mla == null) {
                _mla = new MovieListAdapter(getActivity(), _movies);
                setListAdapter(_mla);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("movies", (ArrayList<? extends Parcelable>) _movies);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.v(TAG, "onCreateView()");
        View view = inflater.inflate(R.layout.fragment_movie_list, container, false);
        if (_mla == null) {
            _getMovieListAsync = new GetMovieListAsyncTask();
            _getMovieListAsync.execute();
        }
        return view;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Log.v(TAG, "onListItemClick()");
        _listener.onMovieItemSelected((Movie) l.getAdapter().getItem(position));
    }

    public interface OnItemSelectedListener {
        public void onMovieItemSelected(Movie movie);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.v(TAG, "onAttach()");
        if (activity instanceof OnItemSelectedListener) {
            _listener = (OnItemSelectedListener) activity;
        } else {
            throw new ClassCastException(activity.toString() + " must implemenet MovieListFragment.OnItemSelectedListener");
        }
    }

    // May also be triggered from the Activity
    public void updateDetail() {
        Log.v(TAG, "updateDetail()");
        // Create fake data
        String newTime = String.valueOf(System.currentTimeMillis());
        // Send data to Activity
        _listener.onMovieItemSelected(null);
    }

    private InputStream retrieveStream(String url) {
        Log.v(TAG, "retrieveStream()");

        DefaultHttpClient client = new DefaultHttpClient();
        HttpGet getRequest = new HttpGet(url);
        try {
            HttpResponse getResponse = client.execute(getRequest);
            final int statusCode = getResponse.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                Log.w(getClass().getSimpleName(), "Error " + statusCode + " for URL " + url);
                return null;
            }
            HttpEntity getResponseEntity = getResponse.getEntity();
            return getResponseEntity.getContent();
        } catch (IOException e) {
            getRequest.abort();
            Log.w(getClass().getSimpleName(), "Error for URL " + url, e);
        }
        return null;
    }

    protected class GetMovieListAsyncTask extends AsyncTask<Void, Void, List<Movie>> {

        private final String TAG = GetMovieListAsyncTask.class.getSimpleName();
        private String rottenTomatoesMovieUrl;

        @Override
        protected void onPreExecute() {
            rottenTomatoesMovieUrl = getString(R.string.rotten_tomatoes_url) + decode(getString(R.string.api_key), getString(R.string.hello_world));
        }

        @Override
        protected List<Movie> doInBackground(Void... arg0) {
            Log.v(TAG, "doInBackground()");

            InputStream source = retrieveStream(rottenTomatoesMovieUrl);
            Gson gson = new Gson();
            Reader reader = new InputStreamReader(source);
            MovieResponse response = gson.fromJson(reader, MovieResponse.class);
            _movies = (ArrayList<Movie>) response.getMovies();
            for (Movie movie : _movies) {
                String url = movie.getPosters().thumbnail;
                Drawable thumbNail = ImageCache.getInstance().getImageFromCache(movie.getPosters().thumbnail);
                try {
                    if (thumbNail == null) {
                        InputStream is = (InputStream) new URL(url).getContent();
                        thumbNail = Drawable.createFromStream(is, "src name");
                        ImageCache.getInstance().addImageToCache(url, thumbNail);
                    }
                    //movie.setMovieThumb(thumbNail);
                } catch (Exception e) {
                    Log.e(TAG, "Something went wrong getting our image.", e);
                }
            }

            return _movies;
        }

        @Override
        protected void onPostExecute(List<Movie> result) {
            super.onPostExecute(result);
            Log.v(TAG, "onPostExecute()");
            _mla = new MovieListAdapter(getActivity(), result);
            setListAdapter(_mla);
        }

        public String encode(String s, String key) {
            return base64Encode(xorWithKey(s.getBytes(), key.getBytes()));
        }

        public String decode(String s, String key) {
            return new String(xorWithKey(base64Decode(s), key.getBytes()));
        }

        private byte[] xorWithKey(byte[] a, byte[] key) {
            byte[] out = new byte[a.length];
            for (int i = 0; i < a.length; i++) {
                out[i] = (byte) (a[i] ^ key[i % key.length]);
            }
            return out;
        }

        private byte[] base64Decode(String s) {
            try {
                return Base64.decode(s);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        private String base64Encode(byte[] bytes) {
            return Base64.encodeBytes(bytes).replaceAll("\\s", "");

        }

    }
}
