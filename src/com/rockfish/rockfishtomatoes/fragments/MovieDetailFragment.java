package com.rockfish.rockfishtomatoes.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rockfish.rockfishtomatoes.ImageCache;
import com.rockfish.rockfishtomatoes.R;
import com.rockfish.rockfishtomatoes.model.Movie;

public class MovieDetailFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.row_movie_item, container, false);
        return view;
    }

    public void showMovieDetail(Movie movie) {
        ImageView thumb = (ImageView) getView().findViewById(R.id.movieThumb);
        TextView view = (TextView) getView().findViewById(R.id.movieTitle);
        thumb.setImageDrawable(ImageCache.getInstance().getImageFromCache(movie.getPosters().thumbnail));
        view.setText(movie.getMovieTitle());
    }

}
