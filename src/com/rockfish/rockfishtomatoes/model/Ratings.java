package com.rockfish.rockfishtomatoes.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Ratings implements Parcelable {

    @SerializedName("critics_score")
    public int critics_score;

    public Ratings(Parcel in) {
        readFromParcel(in);
    }

    public static final Parcelable.Creator<Ratings> CREATOR = new Parcelable.Creator<Ratings>() {

        @Override
        public Ratings createFromParcel(Parcel source) {
            return new Ratings(source);
        }

        @Override
        public Ratings[] newArray(int size) {
            return new Ratings[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.critics_score);
    }

    private void readFromParcel(Parcel in) {
        this.critics_score = in.readInt();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + critics_score;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Ratings))
            return false;
        Ratings other = (Ratings) obj;
        if (critics_score != other.critics_score)
            return false;
        return true;
    }

}
