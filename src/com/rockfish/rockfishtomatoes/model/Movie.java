package com.rockfish.rockfishtomatoes.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Movie implements Parcelable {

    @SerializedName("posters")
    private Posters posters;

    @SerializedName("title")
    private String movieTitle;

    @SerializedName("ratings")
    private Ratings ratings;

    @SerializedName("mpaa_rating")
    private String mpaaRating;

    public Movie(Posters posters, String movieTitle, Ratings ratings) {
        super();
        this.posters = posters;
        this.movieTitle = movieTitle;
        this.ratings = ratings;
    }

    public Posters getPosters() {
        return posters;
    }

    public void setPosters(Posters posters) {
        this.posters = posters;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public Ratings getRatings() {
        return ratings;
    }

    public void setRatings(Ratings ratings) {
        this.ratings = ratings;
    }

    public String getMpaaRating() {
        return mpaaRating;
    }

    public void setMpaaRating(String mpaaRating) {
        this.mpaaRating = mpaaRating;
    }

    public Movie(Parcel in) {
        readFromParcel(in);
    }

    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {

        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.movieTitle);
        dest.writeParcelable(this.posters, 0);
        dest.writeParcelable(this.ratings, 0);
        dest.writeString(this.mpaaRating);
    }

    private void readFromParcel(Parcel in) {
        this.movieTitle = in.readString();
        this.posters = in.readParcelable(Posters.class.getClassLoader());
        this.ratings = in.readParcelable(Ratings.class.getClassLoader());
        this.mpaaRating = in.readString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((movieTitle == null) ? 0 : movieTitle.hashCode());
        result = prime * result + ((mpaaRating == null) ? 0 : mpaaRating.hashCode());
        result = prime * result + ((posters == null) ? 0 : posters.hashCode());
        result = prime * result + ((ratings == null) ? 0 : ratings.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Movie))
            return false;
        Movie other = (Movie) obj;
        if (movieTitle == null) {
            if (other.movieTitle != null)
                return false;
        } else if (!movieTitle.equals(other.movieTitle))
            return false;
        if (mpaaRating == null) {
            if (other.mpaaRating != null)
                return false;
        } else if (!mpaaRating.equals(other.mpaaRating))
            return false;
        if (posters == null) {
            if (other.posters != null)
                return false;
        } else if (!posters.equals(other.posters))
            return false;
        if (ratings == null) {
            if (other.ratings != null)
                return false;
        } else if (!ratings.equals(other.ratings))
            return false;
        return true;
    }

}
