package com.rockfish.rockfishtomatoes;

import com.testflightapp.lib.TestFlight;

import android.app.Application;

public class RockfishTomatoesApplication extends Application {
	@Override
	public void onCreate() {
		super.onCreate();
		// Initialize TestFlight with your app token.
		TestFlight.takeOff(this, "f22df51b-de05-41d7-bdbc-3497ad698391");
	}
}
